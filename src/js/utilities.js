export function capitalizeFirstLetter(item) {
    return item.charAt(0).toUpperCase() + item.slice(1);
}
export function generateFullName(item) {
    return item.firstName + " " + item.lastName;
}

export function doesArrayContains(students, objectInUse) {
    let arrayToReturn = [];
    students.forEach((student) => {
        let objectToREturn = Object.assign({}, student);
        objectInUse.arrayToUse.forEach(element => {
            if (element === objectToREturn.id) {
                objectToREturn.attendanceMark[objectInUse.propertyName] = true;
                objectToREturn.inMultipleBasket = false;
            }
        });
        arrayToReturn.push(objectToREturn)
    }
    )
    return arrayToReturn;
}