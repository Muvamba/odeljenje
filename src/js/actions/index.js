import {RESET_STUDENTS,UPDATE_STUDENT, GET_STUDENT, UPDATE_MULTIPLE_STUDENTS } from "../constants/action-types";
export const getStudent = id => ({ type: GET_STUDENT, payload: id });
export const resetStudents = reset => ({ type: RESET_STUDENTS, payload: reset });
export const updateStudent = student => ({ type: UPDATE_STUDENT, payload: student });
export const updateMultipleStudents = objectToSend => ({ type: UPDATE_MULTIPLE_STUDENTS, payload: objectToSend });