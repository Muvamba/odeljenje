import React from "react";
import Ucionica from "../container/Ucionica";
import { STUDENTSTATES} from "../../constants/constants";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return { students: state.students };
};

const StartingContainerInner = ({ students }) => (
  <div className="classroom-dashboard">
    <Ucionica ucionicaProps={generatePropsForUcionica(students)} />
  </div>
);

function generatePropsForUcionica(students) {
  let ucionicaProps = {
    studentsListInUse: [],
    present: [],
    late: [],
    absent: [],
    unmarked: [],
    multipleStudentsBasket: []
  };

  ucionicaProps.studentsListInUse = _.cloneDeep(students);
  ucionicaProps.studentsListInUse.forEach((student) => {
    let checkedSomwhere = false;
    STUDENTSTATES.forEach((state, i) => {
      if (student.attendanceMark[state]) {
        ucionicaProps[state].push(student.id);
        checkedSomwhere = true;
        student.locked = state;
      }
    });
    if (!checkedSomwhere) {
      ucionicaProps.unmarked.push(student.id)
      student.locked = 'unmarked';
    }
    if (student.inMultipleBasket === undefined) {
      student.inMultipleBasket = false;
    } else if (student.inMultipleBasket) {
      ucionicaProps.multipleStudentsBasket.push(student.id)
    }
  })
  return ucionicaProps;
}

const StartingContainer = connect(mapStateToProps)(StartingContainerInner);

export default StartingContainer;

