import React from "react";
import PropTypes from "prop-types";
import Summary from "../presentational/Summary";

const SummaryForm = ({ summaryList, updateSubmited, showModal, stateOfClassroom, resetStateOfClassroom }) => (
    <div className='summary-form'>
        <div className='summary-form-content'>
            <Summary summaryList={summaryList} summaryState={stateOfClassroom} />
            <div className="summary-form-buttons">
                {renderResetButton(updateSubmited, resetStateOfClassroom)}
                {renderDoneButton(updateSubmited, showModal)}
            </div>
        </div>
    </div>
);

function renderResetButton(updateSubmited, resetStateOfClassroom) {
    const resetButtonClasses = "btn summary-form-button";

    return (
        <div>
            <div
                className={resetButtonClasses}
                onClick={() => { resetButtonClicked(resetStateOfClassroom) }}>
                Reset
            </div>
        </div>
    );
}

function renderDoneButton(updateSubmited, showModal) {
    const resetButtonClasses = 'btn summary-form-button';
    const saveButtonIcon = `fa ${updateSubmited ? "fa-spin fa-spinner" : "fa-save"}`;

    return (
        <div>
            <div className={resetButtonClasses}
                onClick={() => { doneButtonClicked(showModal) }}
            >
                <i className={saveButtonIcon}/>
                Done
            </div>
        </div>
    );
}

function resetButtonClicked(resetStateOfClassroom) {
    resetStateOfClassroom();
}
function doneButtonClicked(renderToggleModal) {
    renderToggleModal();
}

SummaryForm.propTypes = {
    showModal: PropTypes.func.isRequired,
    updateSubmited: PropTypes.bool,
    stateOfClassroom: PropTypes.object.isRequired,
    resetStateOfClassroom: PropTypes.func.isRequired,
    summaryList: PropTypes.array.isRequired
};

export default SummaryForm;