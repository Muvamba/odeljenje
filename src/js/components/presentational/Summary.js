import React from "react";
import PropTypes from "prop-types";
import {capitalizeFirstLetter} from "../../utilities";

const Summary = ({ summaryList, summaryState }) => (
    <div className='summary-content'>
        {
            summaryList.map((listItem,i) => {

                return (
                    <div className="summary-form-item" key ={i}>
                        <div>{capitalizeFirstLetter(listItem)}</div>
                        <div>{summaryState[listItem]}</div>
                    </div>
                );
            })
        }
    </div>
);

Summary.propTypes = {
    summaryList: PropTypes.array.isRequired,
    summaryState: PropTypes.object.isRequired
};

export default Summary;