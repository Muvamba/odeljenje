import React from "react";
import PropTypes from "prop-types";

const StudentFooter = ({ student, onClickCardElement }) => (
    <div className="card-controls">
        <div
            className="card-btn fa fa-check "
            onClick={() => { studentOnClickPresent(student.id, student.locked, onClickCardElement, true) }}
        />
        <div
            className="card-btn 	fa fa-clock-o"
            onClick={() => { studentOnClickPresent(student.id, student.locked, onClickCardElement, false, true) }}
        />
        <div
            className="card-btn 	fa fa-close"
            onClick={() => { studentOnClickPresent(student.id, student.locked, onClickCardElement, false, false, true) }}
        />
    </div>
);


function studentOnClickPresent(studentId, locked, onClickCardElement, present, late) {
    if (present) {
        onClickCardElement(studentId, locked, 'present');
    } else if (late) {
        onClickCardElement(studentId, locked, 'late');
    } else {
        onClickCardElement(studentId, locked, 'absent');
    }
}

StudentFooter.propTypes = {
    student: PropTypes.object.isRequired,
    onClickCardElement: PropTypes.func.isRequired
};

export default StudentFooter;