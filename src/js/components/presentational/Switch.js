import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

class Switch extends Component {
    constructor(props = {}) {
        super();
        this.state = {
            value: false,
            animate: false
        };

        this.animate = false;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.animate = true;
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.value === this.props.value) {
            this.animate = false;
        }
    }

    render() {
        const { id, value, async } = this.props;

        const sliderClasses = classNames({
            "slider": true,
            [Switch.getClass("slider", value)]: true,
            "animate": this.animate
        });

        const asyncClasses = classNames({
            "async-spinner": true,
            [Switch.getClass("async-spinner", value)]: true
        });

        return (
            <div className="switch-wrapper">
                <div className="switch">
                    <input id={id} type="checkbox" checked={!!value} onChange={this.onClickHandler.bind(this)} />
                    <label htmlFor={id} className={labelClasses}>
                        <div className={sliderClasses} />
                        {async ? <div className={asyncClasses} /> : null}
                    </label>
                </div>
            </div>
        );
    }

    onClickHandler(event) {
        event.stopPropagation();
        if (this.props.onChange) {
            this.props.onChange(event.target.checked);
        }
    }
    static getClass(prefix, value) {
        return value ? `${prefix}--on` : `${prefix}--off`;
    }
}

Switch.propTypes = {
    id: PropTypes.any.isRequired,
    onChange: PropTypes.func,
    value: PropTypes.any
};

export default Switch;
