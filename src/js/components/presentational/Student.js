import React from "react";
import PropTypes from "prop-types";
import StudentFooter from "../presentational/StudentFooter";
import { STUDENTSTATES } from "../../constants/constants";
import { capitalizeFirstLetter } from "../../utilities";

const Student = ({ student, onClickCardElement, multiselect, studentClass }) => (
    <div
        key={student.Id}
        className={studentClass}
        onClick={() => { onMultiselectClick(student, onClickCardElement, multiselect) }}
    >
        <div className='student-card-image-wrapper'>
            <img src={student.image}  alt="Student image" />
        </div>
        <div className='student-card-name'><span>{student.firstName}</span> {student.lastName}</div>
        {renderFooter(student, onClickCardElement, multiselect)}
    </div>
);

function renderFooter(student, onClickCardElement, multiselect) {
    return (student.locked === 'unmarked' && !multiselect) ?
        (
            <StudentFooter
                student={student}
                indexOfAttendanceMark={student.locked}
                onClickCardElement={onClickCardElement}
                multiselect={multiselect}
            />
        ) :
        (renderSelectedFooter(student.locked))
}

function renderSelectedFooter(indexOfAttendanceMark) {
    switch (indexOfAttendanceMark) {
        case 'present':
            return (<div className='card-controls'>
                <div className="card-btn-checked">
                    <div className="fa fa-check" />
                    {capitalizeFirstLetter(STUDENTSTATES[0])}
                </div>
            </div>);
        case 'late':
            return (<div className='card-controls'>
                <div className="card-btn-checked">
                    <div className="fa fa-clock-o" />
                    {capitalizeFirstLetter(STUDENTSTATES[1])}
                </div>
            </div>);
        case 'absent':
            return (<div className='card-controls'>
                <div className="card-btn-checked">
                    <div className="fa fa-close" />
                    {capitalizeFirstLetter(STUDENTSTATES[2])}
                </div>
            </div>);
    }
}

function onMultiselectClick(student, onClickCardElement, multiselect) {
    if (multiselect) {
        onClickCardElement(student.id, "unmarked");
    }
}

Student.propTypes = {
    student: PropTypes.object.isRequired,
    onClickCardElement: PropTypes.func.isRequired,
    multiselect: PropTypes.bool.isRequired,
    studentClass: PropTypes.string
};

export default Student;