import React, { Component } from "react";
import PropTypes from "prop-types";
import Student from "../presentational/Student";
import StudentFooter from "../presentational/StudentFooter";
import Modal from "../container/Modal";
import SummaryForm from "../presentational/SummaryForm";
import Switch from "react-switch";
import { SUMMARY_LIST} from "../../constants/constants";
import * as _ from 'lodash';
import { connect } from "react-redux";
import { addStudent, resetStudents, updateStudent, updateMultipleStudents } from "../../actions/index";

const mapDispatchToProps = dispatch => {
    return {
        addStudent: student => dispatch(addStudent(student)),
        updateStudent: student => dispatch(updateStudent(student)),
        resetStudents: reset => dispatch(resetStudents(reset)),
        updateMultipleStudents : objectToSend => dispatch(updateMultipleStudents(objectToSend)),
    };
};

class PovezanaUcionica extends Component {
    constructor() {
        super();
        this.state = {
            switchChecked: false,
            showModal: false,
            multipleStudentsBasket: []
        };

        this.onClickCardElement = this.onClickCardElement.bind(this);
        this.renderStudent = this.renderStudent.bind(this);
        this.updateStudent = this.updateStudent.bind(this);
        this.resetState = this.resetState.bind(this);
        this.showModal = this.showModal.bind(this);
        this.onCloseModalRequest = this.onCloseModalRequest.bind(this);
        this.renderModal = this.renderModal.bind(this);
        this.handleSwitch = this.handleSwitch.bind(this);
        this.fillPresenceFromBasket = this.fillPresenceFromBasket.bind(this);
        this.elementFromTheBasket = this.elementFromTheBasket.bind(this);
    }

    resetState() {
        this.props.resetStudents(true)
    }

    showModal() {
        this.setState(prevState => ({
            showModal: !prevState.showModal
        }));
    }

    onCloseModalRequest() {
        this.showModal();   
    }

    handleSwitch(switchChecked) {
        this.setState({ switchChecked: switchChecked });
    }

    onClickCardElement(id, locked, propertyName) {
            if (locked === 'unmarked') {
                let student = this.props.ucionicaProps.studentsListInUse.filter((student) => {
                    return student.id === id;
                });
                let studentToUpdate = this.updateStudent(student[0], propertyName);
                this.props.updateStudent(studentToUpdate);
            }
    }

    fillPresenceFromBasket(id, locked, propertyName) {
        let objectToSend= {
            propertyName: propertyName,
            arrayToUse: this.props.ucionicaProps.multipleStudentsBasket
        }
        this.props.updateMultipleStudents(objectToSend);
        }

    elementFromTheBasket(array, propertyName){
        let arrayInUse = [...array];
        this.props.ucionicaProps.studentsListInUse.forEach((student)=>{
            if(arrayInUse[0] === student.id){
                student.inMultipleBasket = false;
                student.attendanceMark[propertyName] = true;
                this.props.updateStudent(student, ()=>{
                    if(arrayInUse>0){
                        arrayInUse.shift();
                        this.elementFromTheBasket(array, propertyName)
                    }
                })
            }
        })
    }

    updateStudent(studentInUse, propertyName) {
        if (!this.state.switchChecked) {
            studentInUse.attendanceMark[propertyName] = true;
            studentInUse.inMultipleBasket = false;
            studentInUse.locked = propertyName;
        } else {
            studentInUse.inMultipleBasket = true;
        }
        return studentInUse;
    }

    renderToggleModal() {
        const stateOfPresence = Object.assign({}, this.state)
        this.props.toggleModal(stateOfPresence);
    }

    renderMultiselectStudentFooter() {
        return (this.state.switchChecked) ? (
            <div className="classroom-header-student-footer">
                <StudentFooter
                    student={{ id: 0 }}
                    onClickCardElement={this.fillPresenceFromBasket}
                    multiselect={this.state.switchChecked}
                />
            </div>
        ) : <div className="classroom-header-student-footer"></div>;
    }

    renderModal() {
        return (this.state.showModal) ? (
            <Modal onCloseRequest={this.onCloseModalRequest}
                ucionicaPropsObjectInUse={this.props.ucionicaProps}
                listOfStates={SUMMARY_LIST} />
        ) : null
    }

    renderStudent(student) {
        const studentCardTypeClasses = "student-card";
        const studentCardTypeClassesLocked = "student-card-locked";
        const studentCardTypeClassesMultipleChosen = "student-card-multiple-chosen";
        const { switchChecked } = this.state;
        let studentClass = (student.locked !== 'unmarked') ? studentCardTypeClassesLocked : studentCardTypeClasses;
        if (switchChecked && student.inMultipleBasket) {
            studentClass = studentCardTypeClassesMultipleChosen;
        }
        return (
            <Student
                studentClass={studentClass}
                multiselect={switchChecked}
                student={student}
                key={student.id}
                onClickCardElement={this.onClickCardElement} />
        );
    }

    render() {
        const stateOdPresence = {
            present: this.props.ucionicaProps.present.length,
            late: this.props.ucionicaProps.late.length,
            absent: this.props.ucionicaProps.absent.length,
            unmarked: this.props.ucionicaProps.unmarked.length
        };
        return (
            <div className="classroom-fullwidth-gray">
                <div className="classroom-header">
                    <div className="classroom-header-switch">
                        <div className="classroom-header-switch-label">Single / Multi </div>
                        <div className="classroom-header-switch-element">
                            <Switch
                                onChange={this.handleSwitch}
                                checked={this.state.switchChecked}
                                id="header-switch"
                            />
                        </div>
                        {this.renderMultiselectStudentFooter()}
                    </div>

                </div>
                <div className="student-cards-wrapper">
                    {
                        this.props.ucionicaProps.studentsListInUse.map((student, i) => {
                            return this.renderStudent(student, i)

                        })
                    }
                </div>
                <SummaryForm
                    summaryList={SUMMARY_LIST}
                    stateOfClassroom={stateOdPresence}
                    resetStateOfClassroom={this.resetState}
                    showModal={this.showModal}
                />
                {
                    this.renderModal(this.state.showModal)
                }
            </div>
        );
    }
}

PovezanaUcionica.propTypes = {
    ucionicaProps: PropTypes.object.isRequired
  };

const Ucionica = connect(null, mapDispatchToProps)(PovezanaUcionica);

export default Ucionica;
