import React, { Component } from "react";
import { capitalizeFirstLetter, generateFullName } from "../../utilities";
import PropTypes from "prop-types";

class Modal extends Component {
  constructor() {
    super();
    this.state = {
      toggle: false
    }

    this.toggle = this.toggle.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleOutsideClick = this.handleOutsideClick.bind(this);

  }

  componentDidMount() {
    window.addEventListener('keyup', this.handleKeyUp, false);
    document.addEventListener('click', this.handleOutsideClick, false);
  }

  componentWillUnmount() {
    window.removeEventListener('keyup', this.handleKeyUp, false);
    document.removeEventListener('click', this.handleOutsideClick, false);
  }

  handleOutsideClick(e) {
    const { onCloseRequest } = this.props;
    document.removeEventListener('click', this.handleOutsideClick, false);
    onCloseRequest();
  }

  handleKeyUp(e) {
    const { onCloseRequest } = this.props;
    const keys = {
      27: () => {
        e.preventDefault();
        window.removeEventListener('keyup', this.handleKeyUp, false);
        onCloseRequest();
      },
    };

    if (keys[e.keyCode]) { keys[e.keyCode](); }
  }

  toggle(event) {
    this.setState(prevState => ({
      toggle: !prevState.toggle
    }));
  }

  renderListOfStatesObjects(state) {
    let { ucionicaPropsObjectInUse } = this.props;

    return ucionicaPropsObjectInUse[state].map((studentId) => {
      return ucionicaPropsObjectInUse.studentsListInUse.map((student, i) => {

        return (student.id === studentId) ? (
          <div className="modal-row-list-element" key={i}>
            {generateFullName(student)}
          </div>) : null;
      })
    })
  }

  render() {
    const { listOfStates } = this.props;
    return (
      <div className='modal-class'
        key="modal">
        {
          listOfStates.map((state, i) => {
            return (
              <div key={i} className="modal-row">
                <div className="modal-row-header">{capitalizeFirstLetter(state)} </div>
                {this.renderListOfStatesObjects(state)}
              </div>);
          })
        }
      </div>
    );
  }
}

Modal.propTypes = {
  onCloseRequest: PropTypes.func.isRequired,
  ucionicaPropsObjectInUse: PropTypes.object.isRequired,
  listOfStates: PropTypes.array.isRequired
};

export default Modal;
