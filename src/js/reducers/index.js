import {GET_STUDENT, UPDATE_STUDENT, RESET_STUDENTS, UPDATE_MULTIPLE_STUDENTS } from "../constants/action-types";
import { students } from "../constants/constants";
import {doesArrayContains} from "../utilities";

const initialState = {
    students: [...students]
};
const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_STUDENT:
            return { student: Object.assign({}, state.students[action.payload]) };
        case UPDATE_STUDENT:
            return Object.assign({}, {
                students: state.students.map(student => {
                    return student.id === action.payload.id ? Object.assign({}, action.payload) : student;
                })
            });
        case RESET_STUDENTS:
            return _.cloneDeep(initialState);
        case UPDATE_MULTIPLE_STUDENTS:
            return Object.assign({}, {
                students: doesArrayContains(state.students, action.payload)
            });
        default:
            return state;
    }
};

export default rootReducer;