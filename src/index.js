import StartingContainer from "./js/components/presentational/StartingContainer";
import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import store from "./js/store/index";

render(
    <Provider store={store}>
      <StartingContainer />
    </Provider>,
    document.getElementById("dashboard")
  );